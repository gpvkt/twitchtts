# Contributing

When contributing to this repository, please first discuss the change you wish to make via issue, email, or any other method with the owners of this repository before making a change.

## Pull Request Process

Ensure any local config files, install or build dependencies are removed. Update the README.md with details of changes, this includes new configuration variables and useful file locations.
