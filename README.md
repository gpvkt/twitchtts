# Twitch TextToSpeech Bot

![Latest Release](https://img.shields.io/gitlab/v/release/38486705) ![License](https://img.shields.io/gitlab/license/38486705) ![Maintenance](https://img.shields.io/maintenance/yes/2024)

A simple Twitch TTS bot (Web Speech API)

## Description

The goal of this project is to provide a simple to use Text to Speech IRC bot. It's mainly focused on Twitch, but might be easily adapt to other IRC chats as well. Anyway, right now some parts are Twitch specific (like `CAP REQ :twitch.tv/commands twitch.tv/tags`). There are some other projects providing TTS for IRC based chats, like [IRC Radio (TTS)](https://play.google.com/store/apps/details?id=com.earthflare.android.ircradio&hl=en&gl=US), but they often are missing moderation features like Black-/Whitelists or deletion of single messages, before they are read. Therefore I created my own TTS bot, providing those features. Another very important aspect was that you should use almost any device capable running an HTML5-webbrowser as output. Therefore the bot uses a client-server "architecture". You can run the bot - for example - on your IRL server, but use your phone as output device. A mobile only solution always have the disadvantage of loosing messages, e.g. when your mobile connection drops.

The project consits of a very simple IRC client, which monitors the incoming messages. If a valid (sender is not on the blacklist, message is not too long, etc.) `!tts` command is detected the message will send into a queue. Depending on your config it will wait there a few seconds for deletion by you/your moderators. If nobody deletes the message, it will send into another queue. This queue will get fetched by the HTML frontend, which will be delivered by an internal webserver (backend). The HTML frontend will use the [Web Speech API](https://wicg.github.io/speech-api/) included in any modern webbrowser to read the incoming TTS message. When this is done it will report back to the webserver and the message will be removed from the queue.

The server part is written in Python. The TTS part is written in Javascript.

By using Javascript for the actual TTS part it's not only very easy to access the Web Speech API and the underlying Speech features of your OS, it also makes it possible to use a wide range of devices to actually play the TTS output. You can start the backend on your PC/Server and open the frontend on your Android/iOS tablet or Mobile Phone. If you expose the backend to the internet (I would recommend to use a reverse proxy, rather than exposing the backend directly) you can also use the TTS bot on the go (e.g. your IRL setup).

## Getting Started

### Dependencies

* Browser with Web Speech API support
* Web Speech API Voices (usually already included in your OS and/or browser)
* If you use `tts.py` see `requirements.txt`. If you use `tts.exe` all dependencies are included.

### Installing

1. Clone the repo, or download and unzip the newest [Release](https://gitlab.com/gpvkt/twitchtts/-/releases)
2. Rename/copy `config-dist.yml` to `config.yml`
3. Adapt `config.yml` to your needs. See `Configuration` for details.

### Configuration

Please use `UTF-8` as encoding, when editing `config.yml`.

Example:

``` lang=yaml
irc:
  channel: "#gpkvt"
  username: "ttsbot"
  oauth_token: "oauth:ohkoace0wooghue8she9xaN0nooSau"
  server: "irc.chat.twitch.tv"
  clearmsg_timeout: 10

http:
  port: 80
  bind: "localhost"

bot:
  start_enabled: True
  subonly: False
  modonly: False
  message_length: 200
  language: de

features:
  quote: True
  pick: True
  vote: True
  wiki: True
  wikitts: True
  wikisentences: 3
  random: True
  version: True
  ping: True

messages:
  toff: "TTS is now inactive."
  ton: "TTS is now active."
  wikitoff: "Wiki TTS is now inactive."
  wikiton: "Wiki TTS is now active."
  too_long: "Sorry, your TTS message is too long."
  disabled: "Sorry, TTS is disabled right now."
  denied: "Sorry, you are not allowed to use TTS."
  subonly: "Sorry, TTS is a sub-only feature."
  whitelist: "Sorry, you are not allowed to use TTS."
  ready: "TTS bot alpha ready!"
  says: "says"
  votestart: "Quickvote started. Send #yourchoice to participate."
  voteend: "Quickvote ended. The results are:"
  votenobody: "Nobody casted a vote. :("
  voteresult: "Voting has ended. The result is:"
  votes: "Votes"
  pickstart: "Pick started. Send #pickme to participate."
  pickresult: "Pick ended. The results are:"
  picknone: "Nobody was picked. :("
  quotenotfound: "Sorry, no quote found."
  quoteaddedprefix: "Quote:"
  quoteaddedsuffix: "added."
  wiki_too_many: "Sorry, there are too many possible results. Try a more narrow search."
  wiki_no_result: "Sorry, there was an error fetching the wikipedia answer."

log:
  level: "INFO"

usermapping:
  gpkvt: "gpk"

whitelist:

```

#### Explanation

##### irc

* `channel`: Channel you want to monitor (e.g. #gpkvt)
* `username`: The bots username (e.g. gpkvt)
* `oauth_token`: The bots OAUTH-Token (e.g. oauth:ohkoace0wooghue8she9xaN0nooSau)
* `server`: Twitch IRC server to be used (default should be fine)
* `clearmsg_timeout`: Time to wait for an moderator to delete a message, before it's added to the TTS queue

You can generate your `oauth_token` by leaving the value empty when starting `tts.exe/tts.py`. The integrated webserver will then provide an OAuth-Generator. Due to limitations to the `redirect_url` parameter used by twitch, this is only possible if you use Port `8080` or `80` as `http:bind`. If you use a different port, you will need to use another [Twitch OAuth Generator](https://html.duckduckgo.com/html/?q=twitch+oauth+token+generator). The bot will need `chat:edit` and `chat:read` permissions.

Please note that the `oauth_token` is valid for approximately 60 days. If it become invalid the bot will not connect anymore and you will have to renew the token.

##### http

* `port`: Internal Webserver Port to listen to (e.g. 8080)
* `bind`: Interface/IP to bind server to (e.g. localhost)

##### bot

* `start_enabled`: Enable the bot on start? If `False` you need to use `!ton` first to make TTS work.
* `subonly`: If `True` only Subs can use TTS
* `modonly`: If `True` only Mods can use TTS
* `message_length`: Maximum allowed message length for TTS
* `language`: Language for `!wiki` command

##### features

* `quote`: Enable/Disable `!quote` function
* `pick`: Enable/Disable `!pick` function
* `wiki`: Enable/Disable `!wiki` function
* `wikitts`: Enable/Disable audio output for `!wiki` function
* `wikisentences`: Number of sentences fetched by `!wiki` function
* `random`: Enable/Disable `!random` function
* `version`: Enable/Disable `!version` function
* `ping`: Enable/Disable `!ping` function

Please note, the text output from `!wiki` might been shorter that the TTS output. This is due to the Twitch chat message length restriction. Therefore the `wikisentences` option might not have any effects, when `wikitts` is set to `False`.

Please also note that every `.` in the wikipedia result is counted as a sentence. This might cause issues with certain date-formats, e.g. if `wikisentences` is set to `1` and `bot:language` is set to `de` the command `!wiki douglas adams` will only return:

 ```text
 Douglas Noël Adams (* 11.
 ```

 Therefore the recommended minimum value for `wikisentences` is `3`.

##### messages

* `toff`: The bots reply when `!toff` is used.
* `ton`: The bots reply when `!ton` is used.
* `wikitoff`: The bots reply when `!wikitoff` is used.
* `wikiton`: The bots reply when `!wikiton` is used.
* `too_long`: The bots reply if message exceeds `message_length`
* `disabled`: The bots reply if TTS is disabled
* `denied`: The bots reply if the user is not allowed to use TTS
* `subonly`: The bots reply if `subonly` is active and the user isn't one.
* `whitelist`: The bots reply if `whitelist` is set and user isn't on the list.
* `ready`: The bots init message
* `says`: Prefix to add between username and message
* `votestart`: Message when a quickvote was started.
* `voteend`: Message when a quickvote ends.
* `votenobody`: Message when quickvote ends, but nobody has voted.
* `voteresult`: Prefix for the result (will be read out).
* `votes`: Suffix to vote count.
* `pickstart`: Message when `!pick` was started.
* `pickresult`: Message when `!pick` ends.
* `picknone`: Message if nobody was picked.
* `quotenotfound`: Message if requests quote wasn't found.
* `quoteaddedprefix`: Prefix for `Quote <number> added` message.
* `quoteaddedsuffix`: Suffix for `Quote <number> added` message.
* `wiki_too_many`: Message if `!wiki` command has more than one result.
* `wiki_no_result`: Message if `!wiki` command hasn't a valid result.

##### log

* `level`: The loglevel, valid values are: `DEBUG`, `INFO`, `WARNING`, `ERROR`, `CRITICAL`

Do not use `DEBUG` in a production environment.

##### usermapping

Use this section to define key:value pairs of usernames. The first value is the Twitch username, the second value is how the bot should pronouce the user, when reading the message. This is helpfull if you have regulars with numbers or strangs chars in the name. You can add new/change entries on the fly without restarting the bot (changes took up to 60 seconds).

Please note: The key (real username) MUST be lowercase.

##### whitelist

You can add a whitelist section to `config.yml`, a whitelist will override any other settings like `subonly` and `modonly`. Only users on the whitelist are allowed to use `!tts`. Broadcasters and mods can temporarily add users (including themselfs) to the whitelist by using the `!ptts` command, though.

A whitelist looks as follows:

``` lang=yaml
whitelist:
  - gpkvt
  - foo
  - bar
```

To disable the whitelist, remove it from `config.yml` completely. If you just leave `whitelist:` without entries, everyone must be whitelisted using `!ptts` (even broadcaster and mods). The permit is temporary until the bot restarts or (whichever happens first) if the user is removed from the whitelist using `!dtts`.

Please note: Usernames MUST be lowercase.

### Executing program

Execute `tts.exe` (or `tts.py` if you have Python installed), open the TTS webpage in your browser (the URL depends on your `bind` and `port` configuration, usually it's just `http://localhost`). Click the `Init` button at the button of the TTS webpage (you should hear `Init complete`).

Connect to the configured Twitch channel and send a message starting with `!tts`. After a few seconds (depending on your `clearmsg_timeout` config), the message should be read.

### Additional Commands

Additional commands (broadcaster and mods only) are:

* `!ping`: Check if bot is alive (the bot should reply: `Pong!`)
* `!version`: Print the bot version
* `!toff`: Turn TTS off (will also empty the current TTS queue)
* `!ton`: Turn TTS back on
* `!wton`: Turn on TTS for `!wiki`
* `!wtoff`: Turn off TTS for `!wiki`
* `!dtts <username>`: Disable TTS for the given user
* `!ptts <username>`: Allow TTS for the given user
* `!usermap <username> <spoken name>`: Add an entry to the usermapping in `config.yml`
* `!delay <int>`: Adjust the `clearmsg_timeout` in `config.yml`

### Additional features

You can enable/disable any feature in your `config.yml`:

``` lang=yaml
features:
  quote: False
  pick: False
  vote: False
  wiki: False
  wikitts: False
  random: False
  version: True
  ping: True
```

The default is enabled.

#### !quickvote

A simple vote system.

**Usage:**

1. Broadcaster/Mods: `!quickvote <message>`
2. User: `#choice`
3. Broadcaster/Mods: `!quickvote`

**Example:**

``` lang=text
mod: !quickvote Is pizza hawaii any good? #yes/#no
chat: #no
mod: !quickvote
bot: The result is: #no
```

If a message is given by the Mods it will be repeated every 60 seconds, so everyone keeps in mind, that a vote is still active.

#### !pick

Randomly choose users from chat (who wants to participate).

**Usage:**

1. Broadcaster/Mods: `!pick <int>`
2. User: #pickme
3. Broadcaster/Mods: `!pick`

**Example:**

``` lang=text
mod: !pick 3
user1: #pickme
user2: #pickme
user3: #pickme
user4: #pickme
user5: #pickme
mod: !pick
bot: The picked users are: user5, user3, user1
```

#### !random

Picks a random line from a file.

**Usage:**

* Broadcaster/Mod: `!random <file>`

You can use multiple files, if you call `!random foo` the bot fetch the random line from a file called `random_foo.txt`, `!random bar` will use the file `random_bar.txt` and so on. If no `<file>` is given the command use the file `random.txt`.

**Example:**

``` lang=text
mod: !random
bot: This is a random message
```

#### !addquote

The `!addquote` command adds a new line to `quotes.txt`.

**Usage:**

* Chat: `!addquote <username> <quote>`

**Example:**

``` lang=text
chat: !addquote gpkvt This is a very funny quote.
bot: Quote #1 was added.
```

#### !smartquote / !sq

Picks a random/specific line from `quotes.txt`.

**Usage:**

* Chat: `!smartquote`
* Chat: `!smartquote <linenumber>`
* Chat: `!smartquote <searchstring>`
* Chat: `!sq`
* Chat: `!sq <linenumber>`
* Chat: `!sq <searchstring>`

(`!sq` is an alias for `!smartquote`)

**Example:**

``` lang=text
chat: !smartquote
bot: This is a random quote
chat: !smartquote 1000
bot: This is quote #1000
chat: !smartquote something stupid
bot: This is a quote containing something stupid (or similiar)
```

The format of `quotes.txt` looks as follows:

``` lang=text
#1: "the quote" -username/game (date)
```

#### !wiki

Search the Wikipedia.

**Usage:**

* Chat: `!wiki <searchstring>`

**Example:**

``` lang=text
chat: !wiki 42 answer
bot: 42 is the "Answer to the Ultimate Question of Life, the Universe, and Everything".
```

## Build

If you prefer to build your own `tts.exe` instead of using the shipped one, you can do as follows:

* Install Python 3
* Install pyinstaller: `pip install pyinstaller`
* Install the required dependencies: `pip install -r requirements.txt -v`
* Create the executeable: `pyinstaller --onefile tts.py`

## Voices

The voices available depend on your Operating System and/or browser. On some systems only a default voice is available and the `Select voice` dropdown might stay empty or will only show entries after you clicked the `Init` button. Some Android devices will show a huge list of voices, but sounds the same no matter which one you choose.

On Windows you can install additional voices via `Settings` >  `Time & language` > `Speech` > `Add voices` or by simply run `Add speech voices`.

Do not use Online-Voices, as this will result in serious delays.

## Help

Feel free to use the [Issuetracker](https://gitlab.com/gpvkt/twitchtts/-/issues) or send an [E-Mail](mailto:contact-project+gpvkt-twitchtts-38486705-issue-@incoming.gitlab.com) if you experience any problems.

## Authors

[@gpkvt](mailto:contact-project+gpvkt-twitchtts-38486705-issue-@incoming.gitlab.com)

## Version History

See [CHANGELOG.md](https://gitlab.com/gpvkt/twitchtts/-/blob/main/CHANGELOG.md)

## License

This project is licensed under the GPLv3 License - see [LICENSE](https://gitlab.com/gpvkt/twitchtts/-/blob/main/LICENSE) for details.

## Acknowledgments

### Ideas and Testing

* [GERBrowny and community](https://www.twitch.tv/gerbrowny/) ![Emote](https://static-cdn.jtvnw.net/emoticons/v2/303172270/static/light/1.0)
* [DerZugger and community](https://www.twitch.tv/derzugger/) ![Emote](https://static-cdn.jtvnw.net/emoticons/v2/302400142/static/light/1.0)
* [Timmeh74 and community](https://www.twitch.tv/timmeh74/) ![Emote](https://static-cdn.jtvnw.net/emoticons/v2/300192675/static/light/1.0)

### Libraries

* [Python](https://www.python.org/)
* [jQuery](https://jquery.org/)
* [Bootstrap](https://getbootstrap.com/)
* [PyYAML](https://pyyaml.org/)
* [requests](https://requests.readthedocs.io/en/latest/)
* [fuzzywuzzy](https://github.com/seatgeek/fuzzywuzzy)
* [wikipedia](https://github.com/goldsmith/Wikipedia)
* [pyinstaller](https://pyinstaller.org/)

## Disclaimer

This project is not affiliated, associated, authorized, endorsed by, or in any way officially connected with Twitch Interactive, Inc.
