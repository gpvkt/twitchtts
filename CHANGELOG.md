# Change Log

All notable changes to this project will be documented in this file. If there is a `Changed` section please read carefully, as this often means that you will need to adapt your `config.yml`.

## [1.8.1] - 2022-10-29

### Added 1.8.1

* Logging to file (tts.log)

### Fixed 1.8.1

* Error when checking black-/whitelist

## [1.8.0] - 2022-10-23

### Added 1.8.0

* Option to disable TTS audio output for !wiki function
* Option to configure the number of sentences fetched by !wiki function
* !wtoff command
* !wton command

### Changed 1.8.0

* "oauth_token doesn't need to start with "oauth:" anymore
* Improved oauth-token detection
* Improved README.md

## [1.7.3] - 2022-10-20

### Fixed 1.7.3

* Crash when message has no/invalid metadata

## [1.7.2] - 2022-10-17

### Fixed 1.7.2

* Crash when evaluating !quickvote result

## [1.7.1] - 2022-10-02

### Fixed 1.7.1

* Allow usage of `!sq` command when TTS is disabled

## [1.7.0] - 2022-08-27

### Added 1.7.0

* Option to disable functions in `config.yml`

## [1.6.1] - 2022-08-25

### Fixed 1.6.1

* Type Error during pick
* Improved command handling

## [1.6.0] - 2022-08-25

### Added 1.6.0

* `!pick` command

## [1.5.0] - 2022-08-24

### Added 1.5.0

* `!wiki` command
* `!version` command

### Changed 1.5.0

* Added `!sq` as alias for `!smartquote`

### Fixed 1.5.0

* Darkmode: Options background color

## [1.4.0] - 2022-08-23

### Added 1.4.0

* `!usermap` command added
* `!delay` command added
* Darkmode added

## [1.3.2] - 2022-08-19

### Fixed 1.3.2

* `!smartquote` and `!addquote` are not longer Mods only.

## [1.3.1] - 2022-08-19

### Added 1.3.1

* Added `game_name` and date to quote

### Fixed (hopefully) 1.3.1

* Improved HTTP request handling (hopefully removes delay in Chrome)

## [1.3.0] - 2022-08-18

### Added 1.3.0

* Added `!smartquote` command
* Added `!addquote` command

## [1.2.5] - 2022-08-16

### Changed 1.2.5

* Improved logging

## [1.2.4] - 2022-08-15

### Added 1.2.4

* Check OAuth Token via Twitch API

### Fixed 1.2.4

* Internal URL when using special HTTP_BIND values

## [1.2.3] - 2022-08-14

### Fixed 1.2.3

* Message sort order

## [1.2.2] - 2022-08-13

### Changed 1.2.2

* The message queue is only queried when the Init button is pressed
* Further code optimization

### Fixed 1.2.2

* Minor fixes

## [1.2.1] - 2022-08-13

### Changed 1.2.1

* Reworked internal code structure

### Fixed 1.2.1

* Publish vote info in chat when reloading config was not working when TTS was disabled
* Casting votes was allowed for broadcaster and mods only

## [1.2.0] - 2022-08-13

### Added

* `!random` feature (see README.md for details)

### Changed 1.2.0

* The vote result will be read out

### Fixed 1.2.0

* Improved handling of missing config values

## [1.1.0] - 2022-08-12

### Added 1.1.0

* `!quickvote` feature (see README.md for details)
* `!ping` command added
* Configoption to start TTS in disabled mode
* OAuth-Token generator
* Webbrowser autostart

### Changed 1.1.0

* You need to review your `config.yml` as there a new config values added.
* The bot replies with a chat message when `!ton` or `!toff` is used

### Fixed 1.1.0

* Improved error handling

## [1.0.0] - 2022-08-11

Initial Release
